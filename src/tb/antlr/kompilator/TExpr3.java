// $ANTLR 3.4 /home/student/git/antlr_swing/src/tb/antlr/kompilator/TExpr3.g 2020-03-26 03:11:24

package tb.antlr.kompilator;


import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

import org.antlr.stringtemplate.*;
import org.antlr.stringtemplate.language.*;
import java.util.HashMap;
@SuppressWarnings({"all", "warnings", "unchecked"})
public class TExpr3 extends TreeParserTmpl {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "DIV", "ELSE", "ID", "IF", "INT", "LP", "MINUS", "MUL", "NL", "PLUS", "PODST", "RP", "THEN", "VAR", "WS"
    };

    public static final int EOF=-1;
    public static final int DIV=4;
    public static final int ELSE=5;
    public static final int ID=6;
    public static final int IF=7;
    public static final int INT=8;
    public static final int LP=9;
    public static final int MINUS=10;
    public static final int MUL=11;
    public static final int NL=12;
    public static final int PLUS=13;
    public static final int PODST=14;
    public static final int RP=15;
    public static final int THEN=16;
    public static final int VAR=17;
    public static final int WS=18;

    // delegates
    public TreeParserTmpl[] getDelegates() {
        return new TreeParserTmpl[] {};
    }

    // delegators


    public TExpr3(TreeNodeStream input) {
        this(input, new RecognizerSharedState());
    }
    public TExpr3(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

protected StringTemplateGroup templateLib =
  new StringTemplateGroup("TExpr3Templates", AngleBracketTemplateLexer.class);

public void setTemplateLib(StringTemplateGroup templateLib) {
  this.templateLib = templateLib;
}
public StringTemplateGroup getTemplateLib() {
  return templateLib;
}
/** allows convenient multi-value initialization:
 *  "new STAttrMap().put(...).put(...)"
 */
public static class STAttrMap extends HashMap {
  public STAttrMap put(String attrName, Object value) {
    super.put(attrName, value);
    return this;
  }
  public STAttrMap put(String attrName, int value) {
    super.put(attrName, new Integer(value));
    return this;
  }
}
    public String[] getTokenNames() { return TExpr3.tokenNames; }
    public String getGrammarFileName() { return "/home/student/git/antlr_swing/src/tb/antlr/kompilator/TExpr3.g"; }


      Integer numer = 0;


    public static class prog_return extends TreeRuleReturnScope {
        public StringTemplate st;
        public Object getTemplate() { return st; }
        public String toString() { return st==null?null:st.toString(); }
    };


    // $ANTLR start "prog"
    // /home/student/git/antlr_swing/src/tb/antlr/kompilator/TExpr3.g:19:1: prog : (e+= expr |d+= decl )* -> template(name=$edeklaracje=$d) \"<deklaracje> start: <name;separator=\" \\n\"> \";
    public final TExpr3.prog_return prog() throws RecognitionException {
        TExpr3.prog_return retval = new TExpr3.prog_return();
        retval.start = input.LT(1);


        List list_e=null;
        List list_d=null;
        RuleReturnScope e = null;
        RuleReturnScope d = null;
        try {
            // /home/student/git/antlr_swing/src/tb/antlr/kompilator/TExpr3.g:19:9: ( (e+= expr |d+= decl )* -> template(name=$edeklaracje=$d) \"<deklaracje> start: <name;separator=\" \\n\"> \")
            // /home/student/git/antlr_swing/src/tb/antlr/kompilator/TExpr3.g:19:11: (e+= expr |d+= decl )*
            {
            // /home/student/git/antlr_swing/src/tb/antlr/kompilator/TExpr3.g:19:11: (e+= expr |d+= decl )*
            loop1:
            do {
                int alt1=3;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==DIV||(LA1_0 >= ID && LA1_0 <= INT)||(LA1_0 >= MINUS && LA1_0 <= MUL)||(LA1_0 >= PLUS && LA1_0 <= PODST)) ) {
                    alt1=1;
                }
                else if ( (LA1_0==VAR) ) {
                    alt1=2;
                }


                switch (alt1) {
            	case 1 :
            	    // /home/student/git/antlr_swing/src/tb/antlr/kompilator/TExpr3.g:19:12: e+= expr
            	    {
            	    pushFollow(FOLLOW_expr_in_prog66);
            	    e=expr();

            	    state._fsp--;

            	    if (list_e==null) list_e=new ArrayList();
            	    list_e.add(e.getTemplate());


            	    }
            	    break;
            	case 2 :
            	    // /home/student/git/antlr_swing/src/tb/antlr/kompilator/TExpr3.g:19:22: d+= decl
            	    {
            	    pushFollow(FOLLOW_decl_in_prog72);
            	    d=decl();

            	    state._fsp--;

            	    if (list_d==null) list_d=new ArrayList();
            	    list_d.add(d.getTemplate());


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            // TEMPLATE REWRITE
            // 19:32: -> template(name=$edeklaracje=$d) \"<deklaracje> start: <name;separator=\" \\n\"> \"
            {
                retval.st = new StringTemplate(templateLib, "<deklaracje> start: <name;separator=\" \\n\"> ",new STAttrMap().put("name", list_e).put("deklaracje", list_d));
            }



            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "prog"


    public static class decl_return extends TreeRuleReturnScope {
        public StringTemplate st;
        public Object getTemplate() { return st; }
        public String toString() { return st==null?null:st.toString(); }
    };


    // $ANTLR start "decl"
    // /home/student/git/antlr_swing/src/tb/antlr/kompilator/TExpr3.g:21:1: decl : ^( VAR i1= ID ) -> dek(n=$ID.text);
    public final TExpr3.decl_return decl() throws RecognitionException {
        TExpr3.decl_return retval = new TExpr3.decl_return();
        retval.start = input.LT(1);


        CommonTree i1=null;

        try {
            // /home/student/git/antlr_swing/src/tb/antlr/kompilator/TExpr3.g:21:7: ( ^( VAR i1= ID ) -> dek(n=$ID.text))
            // /home/student/git/antlr_swing/src/tb/antlr/kompilator/TExpr3.g:22:9: ^( VAR i1= ID )
            {
            match(input,VAR,FOLLOW_VAR_in_decl107); 

            match(input, Token.DOWN, null); 
            i1=(CommonTree)match(input,ID,FOLLOW_ID_in_decl111); 

            match(input, Token.UP, null); 


            globals.newSymbol((i1!=null?i1.getText():null));

            // TEMPLATE REWRITE
            // 22:53: -> dek(n=$ID.text)
            {
                retval.st = templateLib.getInstanceOf("dek",new STAttrMap().put("n", (i1!=null?i1.getText():null)));
            }



            }

        }
        catch (RuntimeException ex) {
            errorID(ex,i1);
        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "decl"


    public static class expr_return extends TreeRuleReturnScope {
        public StringTemplate st;
        public Object getTemplate() { return st; }
        public String toString() { return st==null?null:st.toString(); }
    };


    // $ANTLR start "expr"
    // /home/student/git/antlr_swing/src/tb/antlr/kompilator/TExpr3.g:26:1: expr : ( ^( PLUS e1= expr e2= expr ) -> add(p1=$e1.stp2=$e2.st)| ^( MINUS e1= expr e2= expr ) -> sub(p1=$e1.stp2=$e2.st)| ^( MUL e1= expr e2= expr ) -> mul(p1=$e1.stp2=$e2.st)| ^( DIV e1= expr e2= expr ) -> div(p1=$e1.stp2=$e2.st)| ^( PODST i1= ID e2= expr ) {...}? -> set(p1=$i1.textp2=$e2.st)| ID {...}? -> get(p1=$ID.text)| ^( IF e1= expr e2= expr e3= expr ) -> if_else(cond=$e1.stact1=$e2.stact2=$e3.stiterator=numer.toString())| INT -> int(i=$INT.textj=numer.toString()));
    public final TExpr3.expr_return expr() throws RecognitionException {
        TExpr3.expr_return retval = new TExpr3.expr_return();
        retval.start = input.LT(1);


        CommonTree i1=null;
        CommonTree ID1=null;
        CommonTree INT2=null;
        TExpr3.expr_return e1 =null;

        TExpr3.expr_return e2 =null;

        TExpr3.expr_return e3 =null;


        try {
            // /home/student/git/antlr_swing/src/tb/antlr/kompilator/TExpr3.g:26:9: ( ^( PLUS e1= expr e2= expr ) -> add(p1=$e1.stp2=$e2.st)| ^( MINUS e1= expr e2= expr ) -> sub(p1=$e1.stp2=$e2.st)| ^( MUL e1= expr e2= expr ) -> mul(p1=$e1.stp2=$e2.st)| ^( DIV e1= expr e2= expr ) -> div(p1=$e1.stp2=$e2.st)| ^( PODST i1= ID e2= expr ) {...}? -> set(p1=$i1.textp2=$e2.st)| ID {...}? -> get(p1=$ID.text)| ^( IF e1= expr e2= expr e3= expr ) -> if_else(cond=$e1.stact1=$e2.stact2=$e3.stiterator=numer.toString())| INT -> int(i=$INT.textj=numer.toString()))
            int alt2=8;
            switch ( input.LA(1) ) {
            case PLUS:
                {
                alt2=1;
                }
                break;
            case MINUS:
                {
                alt2=2;
                }
                break;
            case MUL:
                {
                alt2=3;
                }
                break;
            case DIV:
                {
                alt2=4;
                }
                break;
            case PODST:
                {
                alt2=5;
                }
                break;
            case ID:
                {
                alt2=6;
                }
                break;
            case IF:
                {
                alt2=7;
                }
                break;
            case INT:
                {
                alt2=8;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;

            }

            switch (alt2) {
                case 1 :
                    // /home/student/git/antlr_swing/src/tb/antlr/kompilator/TExpr3.g:26:11: ^( PLUS e1= expr e2= expr )
                    {
                    match(input,PLUS,FOLLOW_PLUS_in_expr150); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_expr_in_expr155);
                    e1=expr();

                    state._fsp--;


                    pushFollow(FOLLOW_expr_in_expr159);
                    e2=expr();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    // TEMPLATE REWRITE
                    // 26:73: -> add(p1=$e1.stp2=$e2.st)
                    {
                        retval.st = templateLib.getInstanceOf("add",new STAttrMap().put("p1", (e1!=null?e1.st:null)).put("p2", (e2!=null?e2.st:null)));
                    }



                    }
                    break;
                case 2 :
                    // /home/student/git/antlr_swing/src/tb/antlr/kompilator/TExpr3.g:27:11: ^( MINUS e1= expr e2= expr )
                    {
                    match(input,MINUS,FOLLOW_MINUS_in_expr223); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_expr_in_expr227);
                    e1=expr();

                    state._fsp--;


                    pushFollow(FOLLOW_expr_in_expr231);
                    e2=expr();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    // TEMPLATE REWRITE
                    // 27:73: -> sub(p1=$e1.stp2=$e2.st)
                    {
                        retval.st = templateLib.getInstanceOf("sub",new STAttrMap().put("p1", (e1!=null?e1.st:null)).put("p2", (e2!=null?e2.st:null)));
                    }



                    }
                    break;
                case 3 :
                    // /home/student/git/antlr_swing/src/tb/antlr/kompilator/TExpr3.g:28:11: ^( MUL e1= expr e2= expr )
                    {
                    match(input,MUL,FOLLOW_MUL_in_expr295); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_expr_in_expr301);
                    e1=expr();

                    state._fsp--;


                    pushFollow(FOLLOW_expr_in_expr305);
                    e2=expr();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    // TEMPLATE REWRITE
                    // 28:73: -> mul(p1=$e1.stp2=$e2.st)
                    {
                        retval.st = templateLib.getInstanceOf("mul",new STAttrMap().put("p1", (e1!=null?e1.st:null)).put("p2", (e2!=null?e2.st:null)));
                    }



                    }
                    break;
                case 4 :
                    // /home/student/git/antlr_swing/src/tb/antlr/kompilator/TExpr3.g:29:11: ^( DIV e1= expr e2= expr )
                    {
                    match(input,DIV,FOLLOW_DIV_in_expr369); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_expr_in_expr375);
                    e1=expr();

                    state._fsp--;


                    pushFollow(FOLLOW_expr_in_expr379);
                    e2=expr();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    // TEMPLATE REWRITE
                    // 29:73: -> div(p1=$e1.stp2=$e2.st)
                    {
                        retval.st = templateLib.getInstanceOf("div",new STAttrMap().put("p1", (e1!=null?e1.st:null)).put("p2", (e2!=null?e2.st:null)));
                    }



                    }
                    break;
                case 5 :
                    // /home/student/git/antlr_swing/src/tb/antlr/kompilator/TExpr3.g:30:11: ^( PODST i1= ID e2= expr ) {...}?
                    {
                    match(input,PODST,FOLLOW_PODST_in_expr443); 

                    match(input, Token.DOWN, null); 
                    i1=(CommonTree)match(input,ID,FOLLOW_ID_in_expr447); 

                    pushFollow(FOLLOW_expr_in_expr453);
                    e2=expr();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    if ( !((globals.hasSymbol((i1!=null?i1.getText():null)))) ) {
                        throw new FailedPredicateException(input, "expr", "globals.hasSymbol($ID.text)");
                    }

                    // TEMPLATE REWRITE
                    // 30:73: -> set(p1=$i1.textp2=$e2.st)
                    {
                        retval.st = templateLib.getInstanceOf("set",new STAttrMap().put("p1", (i1!=null?i1.getText():null)).put("p2", (e2!=null?e2.st:null)));
                    }



                    }
                    break;
                case 6 :
                    // /home/student/git/antlr_swing/src/tb/antlr/kompilator/TExpr3.g:31:11: ID {...}?
                    {
                    ID1=(CommonTree)match(input,ID,FOLLOW_ID_in_expr487); 

                    if ( !((globals.hasSymbol((ID1!=null?ID1.getText():null)))) ) {
                        throw new FailedPredicateException(input, "expr", "globals.hasSymbol($ID.text)");
                    }

                    // TEMPLATE REWRITE
                    // 31:73: -> get(p1=$ID.text)
                    {
                        retval.st = templateLib.getInstanceOf("get",new STAttrMap().put("p1", (ID1!=null?ID1.getText():null)));
                    }



                    }
                    break;
                case 7 :
                    // /home/student/git/antlr_swing/src/tb/antlr/kompilator/TExpr3.g:32:11: ^( IF e1= expr e2= expr e3= expr )
                    {
                    match(input,IF,FOLLOW_IF_in_expr539); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_expr_in_expr543);
                    e1=expr();

                    state._fsp--;


                    pushFollow(FOLLOW_expr_in_expr547);
                    e2=expr();

                    state._fsp--;


                    pushFollow(FOLLOW_expr_in_expr551);
                    e3=expr();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    numer++;

                    // TEMPLATE REWRITE
                    // 32:73: -> if_else(cond=$e1.stact1=$e2.stact2=$e3.stiterator=numer.toString())
                    {
                        retval.st = templateLib.getInstanceOf("if_else",new STAttrMap().put("cond", (e1!=null?e1.st:null)).put("act1", (e2!=null?e2.st:null)).put("act2", (e3!=null?e3.st:null)).put("iterator", numer.toString()));
                    }



                    }
                    break;
                case 8 :
                    // /home/student/git/antlr_swing/src/tb/antlr/kompilator/TExpr3.g:36:11: INT
                    {
                    INT2=(CommonTree)match(input,INT,FOLLOW_INT_in_expr862); 

                    numer++;

                    // TEMPLATE REWRITE
                    // 36:73: -> int(i=$INT.textj=numer.toString())
                    {
                        retval.st = templateLib.getInstanceOf("int",new STAttrMap().put("i", (INT2!=null?INT2.getText():null)).put("j", numer.toString()));
                    }



                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "expr"

    // Delegated rules


 

    public static final BitSet FOLLOW_expr_in_prog66 = new BitSet(new long[]{0x0000000000026DD2L});
    public static final BitSet FOLLOW_decl_in_prog72 = new BitSet(new long[]{0x0000000000026DD2L});
    public static final BitSet FOLLOW_VAR_in_decl107 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_ID_in_decl111 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_PLUS_in_expr150 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr155 = new BitSet(new long[]{0x0000000000006DD0L});
    public static final BitSet FOLLOW_expr_in_expr159 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_MINUS_in_expr223 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr227 = new BitSet(new long[]{0x0000000000006DD0L});
    public static final BitSet FOLLOW_expr_in_expr231 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_MUL_in_expr295 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr301 = new BitSet(new long[]{0x0000000000006DD0L});
    public static final BitSet FOLLOW_expr_in_expr305 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_DIV_in_expr369 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr375 = new BitSet(new long[]{0x0000000000006DD0L});
    public static final BitSet FOLLOW_expr_in_expr379 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_PODST_in_expr443 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_ID_in_expr447 = new BitSet(new long[]{0x0000000000006DD0L});
    public static final BitSet FOLLOW_expr_in_expr453 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_ID_in_expr487 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_IF_in_expr539 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expr_in_expr543 = new BitSet(new long[]{0x0000000000006DD0L});
    public static final BitSet FOLLOW_expr_in_expr547 = new BitSet(new long[]{0x0000000000006DD0L});
    public static final BitSet FOLLOW_expr_in_expr551 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_INT_in_expr862 = new BitSet(new long[]{0x0000000000000002L});

}