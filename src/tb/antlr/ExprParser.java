// $ANTLR 3.4 /home/student/git/antlr_swing/src/tb/antlr/Expr.g 2020-03-26 03:11:21

package tb.antlr;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

import org.antlr.runtime.tree.*;


@SuppressWarnings({"all", "warnings", "unchecked"})
public class ExprParser extends Parser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "DIV", "ELSE", "ID", "IF", "INT", "LP", "MINUS", "MUL", "NL", "PLUS", "PODST", "RP", "THEN", "VAR", "WS"
    };

    public static final int EOF=-1;
    public static final int DIV=4;
    public static final int ELSE=5;
    public static final int ID=6;
    public static final int IF=7;
    public static final int INT=8;
    public static final int LP=9;
    public static final int MINUS=10;
    public static final int MUL=11;
    public static final int NL=12;
    public static final int PLUS=13;
    public static final int PODST=14;
    public static final int RP=15;
    public static final int THEN=16;
    public static final int VAR=17;
    public static final int WS=18;

    // delegates
    public Parser[] getDelegates() {
        return new Parser[] {};
    }

    // delegators


    public ExprParser(TokenStream input) {
        this(input, new RecognizerSharedState());
    }
    public ExprParser(TokenStream input, RecognizerSharedState state) {
        super(input, state);
    }

protected TreeAdaptor adaptor = new CommonTreeAdaptor();

public void setTreeAdaptor(TreeAdaptor adaptor) {
    this.adaptor = adaptor;
}
public TreeAdaptor getTreeAdaptor() {
    return adaptor;
}
    public String[] getTokenNames() { return ExprParser.tokenNames; }
    public String getGrammarFileName() { return "/home/student/git/antlr_swing/src/tb/antlr/Expr.g"; }


    public static class prog_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "prog"
    // /home/student/git/antlr_swing/src/tb/antlr/Expr.g:16:1: prog : ( stat )+ EOF !;
    public final ExprParser.prog_return prog() throws RecognitionException {
        ExprParser.prog_return retval = new ExprParser.prog_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token EOF2=null;
        ExprParser.stat_return stat1 =null;


        CommonTree EOF2_tree=null;

        try {
            // /home/student/git/antlr_swing/src/tb/antlr/Expr.g:17:5: ( ( stat )+ EOF !)
            // /home/student/git/antlr_swing/src/tb/antlr/Expr.g:17:7: ( stat )+ EOF !
            {
            root_0 = (CommonTree)adaptor.nil();


            // /home/student/git/antlr_swing/src/tb/antlr/Expr.g:17:7: ( stat )+
            int cnt1=0;
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( ((LA1_0 >= ID && LA1_0 <= LP)||LA1_0==NL||LA1_0==VAR) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // /home/student/git/antlr_swing/src/tb/antlr/Expr.g:17:8: stat
            	    {
            	    pushFollow(FOLLOW_stat_in_prog49);
            	    stat1=stat();

            	    state._fsp--;

            	    adaptor.addChild(root_0, stat1.getTree());

            	    }
            	    break;

            	default :
            	    if ( cnt1 >= 1 ) break loop1;
                        EarlyExitException eee =
                            new EarlyExitException(1, input);
                        throw eee;
                }
                cnt1++;
            } while (true);


            EOF2=(Token)match(input,EOF,FOLLOW_EOF_in_prog54); 

            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "prog"


    public static class stat_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "stat"
    // /home/student/git/antlr_swing/src/tb/antlr/Expr.g:19:1: stat : ( expr NL -> expr | VAR ID NL -> ^( VAR ID ) | ID PODST expr NL -> ^( PODST ID expr ) | if_else NL -> if_else | NL ->);
    public final ExprParser.stat_return stat() throws RecognitionException {
        ExprParser.stat_return retval = new ExprParser.stat_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token NL4=null;
        Token VAR5=null;
        Token ID6=null;
        Token NL7=null;
        Token ID8=null;
        Token PODST9=null;
        Token NL11=null;
        Token NL13=null;
        Token NL14=null;
        ExprParser.expr_return expr3 =null;

        ExprParser.expr_return expr10 =null;

        ExprParser.if_else_return if_else12 =null;


        CommonTree NL4_tree=null;
        CommonTree VAR5_tree=null;
        CommonTree ID6_tree=null;
        CommonTree NL7_tree=null;
        CommonTree ID8_tree=null;
        CommonTree PODST9_tree=null;
        CommonTree NL11_tree=null;
        CommonTree NL13_tree=null;
        CommonTree NL14_tree=null;
        RewriteRuleTokenStream stream_VAR=new RewriteRuleTokenStream(adaptor,"token VAR");
        RewriteRuleTokenStream stream_PODST=new RewriteRuleTokenStream(adaptor,"token PODST");
        RewriteRuleTokenStream stream_ID=new RewriteRuleTokenStream(adaptor,"token ID");
        RewriteRuleTokenStream stream_NL=new RewriteRuleTokenStream(adaptor,"token NL");
        RewriteRuleSubtreeStream stream_expr=new RewriteRuleSubtreeStream(adaptor,"rule expr");
        RewriteRuleSubtreeStream stream_if_else=new RewriteRuleSubtreeStream(adaptor,"rule if_else");
        try {
            // /home/student/git/antlr_swing/src/tb/antlr/Expr.g:20:5: ( expr NL -> expr | VAR ID NL -> ^( VAR ID ) | ID PODST expr NL -> ^( PODST ID expr ) | if_else NL -> if_else | NL ->)
            int alt2=5;
            switch ( input.LA(1) ) {
            case INT:
            case LP:
                {
                alt2=1;
                }
                break;
            case ID:
                {
                int LA2_2 = input.LA(2);

                if ( (LA2_2==PODST) ) {
                    alt2=3;
                }
                else if ( (LA2_2==DIV||(LA2_2 >= MINUS && LA2_2 <= PLUS)) ) {
                    alt2=1;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 2, 2, input);

                    throw nvae;

                }
                }
                break;
            case VAR:
                {
                alt2=2;
                }
                break;
            case IF:
                {
                alt2=4;
                }
                break;
            case NL:
                {
                alt2=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;

            }

            switch (alt2) {
                case 1 :
                    // /home/student/git/antlr_swing/src/tb/antlr/Expr.g:20:7: expr NL
                    {
                    pushFollow(FOLLOW_expr_in_stat67);
                    expr3=expr();

                    state._fsp--;

                    stream_expr.add(expr3.getTree());

                    NL4=(Token)match(input,NL,FOLLOW_NL_in_stat69);  
                    stream_NL.add(NL4);


                    // AST REWRITE
                    // elements: expr
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 20:15: -> expr
                    {
                        adaptor.addChild(root_0, stream_expr.nextTree());

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 2 :
                    // /home/student/git/antlr_swing/src/tb/antlr/Expr.g:22:7: VAR ID NL
                    {
                    VAR5=(Token)match(input,VAR,FOLLOW_VAR_in_stat82);  
                    stream_VAR.add(VAR5);


                    ID6=(Token)match(input,ID,FOLLOW_ID_in_stat84);  
                    stream_ID.add(ID6);


                    NL7=(Token)match(input,NL,FOLLOW_NL_in_stat86);  
                    stream_NL.add(NL7);


                    // AST REWRITE
                    // elements: ID, VAR
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 22:17: -> ^( VAR ID )
                    {
                        // /home/student/git/antlr_swing/src/tb/antlr/Expr.g:22:20: ^( VAR ID )
                        {
                        CommonTree root_1 = (CommonTree)adaptor.nil();
                        root_1 = (CommonTree)adaptor.becomeRoot(
                        stream_VAR.nextNode()
                        , root_1);

                        adaptor.addChild(root_1, 
                        stream_ID.nextNode()
                        );

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 3 :
                    // /home/student/git/antlr_swing/src/tb/antlr/Expr.g:23:7: ID PODST expr NL
                    {
                    ID8=(Token)match(input,ID,FOLLOW_ID_in_stat102);  
                    stream_ID.add(ID8);


                    PODST9=(Token)match(input,PODST,FOLLOW_PODST_in_stat104);  
                    stream_PODST.add(PODST9);


                    pushFollow(FOLLOW_expr_in_stat106);
                    expr10=expr();

                    state._fsp--;

                    stream_expr.add(expr10.getTree());

                    NL11=(Token)match(input,NL,FOLLOW_NL_in_stat108);  
                    stream_NL.add(NL11);


                    // AST REWRITE
                    // elements: ID, expr, PODST
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 23:24: -> ^( PODST ID expr )
                    {
                        // /home/student/git/antlr_swing/src/tb/antlr/Expr.g:23:27: ^( PODST ID expr )
                        {
                        CommonTree root_1 = (CommonTree)adaptor.nil();
                        root_1 = (CommonTree)adaptor.becomeRoot(
                        stream_PODST.nextNode()
                        , root_1);

                        adaptor.addChild(root_1, 
                        stream_ID.nextNode()
                        );

                        adaptor.addChild(root_1, stream_expr.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 4 :
                    // /home/student/git/antlr_swing/src/tb/antlr/Expr.g:25:7: if_else NL
                    {
                    pushFollow(FOLLOW_if_else_in_stat127);
                    if_else12=if_else();

                    state._fsp--;

                    stream_if_else.add(if_else12.getTree());

                    NL13=(Token)match(input,NL,FOLLOW_NL_in_stat129);  
                    stream_NL.add(NL13);


                    // AST REWRITE
                    // elements: if_else
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 25:18: -> if_else
                    {
                        adaptor.addChild(root_0, stream_if_else.nextTree());

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 5 :
                    // /home/student/git/antlr_swing/src/tb/antlr/Expr.g:26:7: NL
                    {
                    NL14=(Token)match(input,NL,FOLLOW_NL_in_stat141);  
                    stream_NL.add(NL14);


                    // AST REWRITE
                    // elements: 
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (CommonTree)adaptor.nil();
                    // 26:10: ->
                    {
                        root_0 = null;
                    }


                    retval.tree = root_0;

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "stat"


    public static class if_else_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "if_else"
    // /home/student/git/antlr_swing/src/tb/antlr/Expr.g:29:1: if_else : IF ^cond= expr THEN !act1= expr ( ELSE !act2= expr )? ;
    public final ExprParser.if_else_return if_else() throws RecognitionException {
        ExprParser.if_else_return retval = new ExprParser.if_else_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token IF15=null;
        Token THEN16=null;
        Token ELSE17=null;
        ExprParser.expr_return cond =null;

        ExprParser.expr_return act1 =null;

        ExprParser.expr_return act2 =null;


        CommonTree IF15_tree=null;
        CommonTree THEN16_tree=null;
        CommonTree ELSE17_tree=null;

        try {
            // /home/student/git/antlr_swing/src/tb/antlr/Expr.g:30:5: ( IF ^cond= expr THEN !act1= expr ( ELSE !act2= expr )? )
            // /home/student/git/antlr_swing/src/tb/antlr/Expr.g:30:7: IF ^cond= expr THEN !act1= expr ( ELSE !act2= expr )?
            {
            root_0 = (CommonTree)adaptor.nil();


            IF15=(Token)match(input,IF,FOLLOW_IF_in_if_else160); 
            IF15_tree = 
            (CommonTree)adaptor.create(IF15)
            ;
            root_0 = (CommonTree)adaptor.becomeRoot(IF15_tree, root_0);


            pushFollow(FOLLOW_expr_in_if_else165);
            cond=expr();

            state._fsp--;

            adaptor.addChild(root_0, cond.getTree());

            THEN16=(Token)match(input,THEN,FOLLOW_THEN_in_if_else167); 

            pushFollow(FOLLOW_expr_in_if_else172);
            act1=expr();

            state._fsp--;

            adaptor.addChild(root_0, act1.getTree());

            // /home/student/git/antlr_swing/src/tb/antlr/Expr.g:30:37: ( ELSE !act2= expr )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==ELSE) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // /home/student/git/antlr_swing/src/tb/antlr/Expr.g:30:38: ELSE !act2= expr
                    {
                    ELSE17=(Token)match(input,ELSE,FOLLOW_ELSE_in_if_else175); 

                    pushFollow(FOLLOW_expr_in_if_else180);
                    act2=expr();

                    state._fsp--;

                    adaptor.addChild(root_0, act2.getTree());

                    }
                    break;

            }


            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "if_else"


    public static class expr_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "expr"
    // /home/student/git/antlr_swing/src/tb/antlr/Expr.g:33:1: expr : multExpr ( PLUS ^ multExpr | MINUS ^ multExpr )* ;
    public final ExprParser.expr_return expr() throws RecognitionException {
        ExprParser.expr_return retval = new ExprParser.expr_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token PLUS19=null;
        Token MINUS21=null;
        ExprParser.multExpr_return multExpr18 =null;

        ExprParser.multExpr_return multExpr20 =null;

        ExprParser.multExpr_return multExpr22 =null;


        CommonTree PLUS19_tree=null;
        CommonTree MINUS21_tree=null;

        try {
            // /home/student/git/antlr_swing/src/tb/antlr/Expr.g:34:5: ( multExpr ( PLUS ^ multExpr | MINUS ^ multExpr )* )
            // /home/student/git/antlr_swing/src/tb/antlr/Expr.g:34:7: multExpr ( PLUS ^ multExpr | MINUS ^ multExpr )*
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_multExpr_in_expr199);
            multExpr18=multExpr();

            state._fsp--;

            adaptor.addChild(root_0, multExpr18.getTree());

            // /home/student/git/antlr_swing/src/tb/antlr/Expr.g:35:7: ( PLUS ^ multExpr | MINUS ^ multExpr )*
            loop4:
            do {
                int alt4=3;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==PLUS) ) {
                    alt4=1;
                }
                else if ( (LA4_0==MINUS) ) {
                    alt4=2;
                }


                switch (alt4) {
            	case 1 :
            	    // /home/student/git/antlr_swing/src/tb/antlr/Expr.g:35:9: PLUS ^ multExpr
            	    {
            	    PLUS19=(Token)match(input,PLUS,FOLLOW_PLUS_in_expr209); 
            	    PLUS19_tree = 
            	    (CommonTree)adaptor.create(PLUS19)
            	    ;
            	    root_0 = (CommonTree)adaptor.becomeRoot(PLUS19_tree, root_0);


            	    pushFollow(FOLLOW_multExpr_in_expr212);
            	    multExpr20=multExpr();

            	    state._fsp--;

            	    adaptor.addChild(root_0, multExpr20.getTree());

            	    }
            	    break;
            	case 2 :
            	    // /home/student/git/antlr_swing/src/tb/antlr/Expr.g:36:9: MINUS ^ multExpr
            	    {
            	    MINUS21=(Token)match(input,MINUS,FOLLOW_MINUS_in_expr222); 
            	    MINUS21_tree = 
            	    (CommonTree)adaptor.create(MINUS21)
            	    ;
            	    root_0 = (CommonTree)adaptor.becomeRoot(MINUS21_tree, root_0);


            	    pushFollow(FOLLOW_multExpr_in_expr225);
            	    multExpr22=multExpr();

            	    state._fsp--;

            	    adaptor.addChild(root_0, multExpr22.getTree());

            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "expr"


    public static class multExpr_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "multExpr"
    // /home/student/git/antlr_swing/src/tb/antlr/Expr.g:40:1: multExpr : atom ( MUL ^ atom | DIV ^ atom )* ;
    public final ExprParser.multExpr_return multExpr() throws RecognitionException {
        ExprParser.multExpr_return retval = new ExprParser.multExpr_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token MUL24=null;
        Token DIV26=null;
        ExprParser.atom_return atom23 =null;

        ExprParser.atom_return atom25 =null;

        ExprParser.atom_return atom27 =null;


        CommonTree MUL24_tree=null;
        CommonTree DIV26_tree=null;

        try {
            // /home/student/git/antlr_swing/src/tb/antlr/Expr.g:41:5: ( atom ( MUL ^ atom | DIV ^ atom )* )
            // /home/student/git/antlr_swing/src/tb/antlr/Expr.g:41:7: atom ( MUL ^ atom | DIV ^ atom )*
            {
            root_0 = (CommonTree)adaptor.nil();


            pushFollow(FOLLOW_atom_in_multExpr251);
            atom23=atom();

            state._fsp--;

            adaptor.addChild(root_0, atom23.getTree());

            // /home/student/git/antlr_swing/src/tb/antlr/Expr.g:42:7: ( MUL ^ atom | DIV ^ atom )*
            loop5:
            do {
                int alt5=3;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==MUL) ) {
                    alt5=1;
                }
                else if ( (LA5_0==DIV) ) {
                    alt5=2;
                }


                switch (alt5) {
            	case 1 :
            	    // /home/student/git/antlr_swing/src/tb/antlr/Expr.g:42:9: MUL ^ atom
            	    {
            	    MUL24=(Token)match(input,MUL,FOLLOW_MUL_in_multExpr261); 
            	    MUL24_tree = 
            	    (CommonTree)adaptor.create(MUL24)
            	    ;
            	    root_0 = (CommonTree)adaptor.becomeRoot(MUL24_tree, root_0);


            	    pushFollow(FOLLOW_atom_in_multExpr264);
            	    atom25=atom();

            	    state._fsp--;

            	    adaptor.addChild(root_0, atom25.getTree());

            	    }
            	    break;
            	case 2 :
            	    // /home/student/git/antlr_swing/src/tb/antlr/Expr.g:43:9: DIV ^ atom
            	    {
            	    DIV26=(Token)match(input,DIV,FOLLOW_DIV_in_multExpr274); 
            	    DIV26_tree = 
            	    (CommonTree)adaptor.create(DIV26)
            	    ;
            	    root_0 = (CommonTree)adaptor.becomeRoot(DIV26_tree, root_0);


            	    pushFollow(FOLLOW_atom_in_multExpr277);
            	    atom27=atom();

            	    state._fsp--;

            	    adaptor.addChild(root_0, atom27.getTree());

            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "multExpr"


    public static class atom_return extends ParserRuleReturnScope {
        CommonTree tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "atom"
    // /home/student/git/antlr_swing/src/tb/antlr/Expr.g:47:1: atom : ( INT | ID | LP ! expr RP !);
    public final ExprParser.atom_return atom() throws RecognitionException {
        ExprParser.atom_return retval = new ExprParser.atom_return();
        retval.start = input.LT(1);


        CommonTree root_0 = null;

        Token INT28=null;
        Token ID29=null;
        Token LP30=null;
        Token RP32=null;
        ExprParser.expr_return expr31 =null;


        CommonTree INT28_tree=null;
        CommonTree ID29_tree=null;
        CommonTree LP30_tree=null;
        CommonTree RP32_tree=null;

        try {
            // /home/student/git/antlr_swing/src/tb/antlr/Expr.g:48:5: ( INT | ID | LP ! expr RP !)
            int alt6=3;
            switch ( input.LA(1) ) {
            case INT:
                {
                alt6=1;
                }
                break;
            case ID:
                {
                alt6=2;
                }
                break;
            case LP:
                {
                alt6=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;

            }

            switch (alt6) {
                case 1 :
                    // /home/student/git/antlr_swing/src/tb/antlr/Expr.g:48:7: INT
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    INT28=(Token)match(input,INT,FOLLOW_INT_in_atom303); 
                    INT28_tree = 
                    (CommonTree)adaptor.create(INT28)
                    ;
                    adaptor.addChild(root_0, INT28_tree);


                    }
                    break;
                case 2 :
                    // /home/student/git/antlr_swing/src/tb/antlr/Expr.g:49:7: ID
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    ID29=(Token)match(input,ID,FOLLOW_ID_in_atom311); 
                    ID29_tree = 
                    (CommonTree)adaptor.create(ID29)
                    ;
                    adaptor.addChild(root_0, ID29_tree);


                    }
                    break;
                case 3 :
                    // /home/student/git/antlr_swing/src/tb/antlr/Expr.g:50:7: LP ! expr RP !
                    {
                    root_0 = (CommonTree)adaptor.nil();


                    LP30=(Token)match(input,LP,FOLLOW_LP_in_atom319); 

                    pushFollow(FOLLOW_expr_in_atom322);
                    expr31=expr();

                    state._fsp--;

                    adaptor.addChild(root_0, expr31.getTree());

                    RP32=(Token)match(input,RP,FOLLOW_RP_in_atom324); 

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "atom"

    // Delegated rules


 

    public static final BitSet FOLLOW_stat_in_prog49 = new BitSet(new long[]{0x00000000000213C0L});
    public static final BitSet FOLLOW_EOF_in_prog54 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_expr_in_stat67 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_NL_in_stat69 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_VAR_in_stat82 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_ID_in_stat84 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_NL_in_stat86 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ID_in_stat102 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_PODST_in_stat104 = new BitSet(new long[]{0x0000000000000340L});
    public static final BitSet FOLLOW_expr_in_stat106 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_NL_in_stat108 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_if_else_in_stat127 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_NL_in_stat129 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_NL_in_stat141 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_IF_in_if_else160 = new BitSet(new long[]{0x0000000000000340L});
    public static final BitSet FOLLOW_expr_in_if_else165 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_THEN_in_if_else167 = new BitSet(new long[]{0x0000000000000340L});
    public static final BitSet FOLLOW_expr_in_if_else172 = new BitSet(new long[]{0x0000000000000022L});
    public static final BitSet FOLLOW_ELSE_in_if_else175 = new BitSet(new long[]{0x0000000000000340L});
    public static final BitSet FOLLOW_expr_in_if_else180 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_multExpr_in_expr199 = new BitSet(new long[]{0x0000000000002402L});
    public static final BitSet FOLLOW_PLUS_in_expr209 = new BitSet(new long[]{0x0000000000000340L});
    public static final BitSet FOLLOW_multExpr_in_expr212 = new BitSet(new long[]{0x0000000000002402L});
    public static final BitSet FOLLOW_MINUS_in_expr222 = new BitSet(new long[]{0x0000000000000340L});
    public static final BitSet FOLLOW_multExpr_in_expr225 = new BitSet(new long[]{0x0000000000002402L});
    public static final BitSet FOLLOW_atom_in_multExpr251 = new BitSet(new long[]{0x0000000000000812L});
    public static final BitSet FOLLOW_MUL_in_multExpr261 = new BitSet(new long[]{0x0000000000000340L});
    public static final BitSet FOLLOW_atom_in_multExpr264 = new BitSet(new long[]{0x0000000000000812L});
    public static final BitSet FOLLOW_DIV_in_multExpr274 = new BitSet(new long[]{0x0000000000000340L});
    public static final BitSet FOLLOW_atom_in_multExpr277 = new BitSet(new long[]{0x0000000000000812L});
    public static final BitSet FOLLOW_INT_in_atom303 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ID_in_atom311 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_LP_in_atom319 = new BitSet(new long[]{0x0000000000000340L});
    public static final BitSet FOLLOW_expr_in_atom322 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_RP_in_atom324 = new BitSet(new long[]{0x0000000000000002L});

}